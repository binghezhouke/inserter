package com.binghezhouke.entity;

import javax.persistence.*;

/**
 * Created by binghezhouke on 16/7/8.
 */
@Entity
@Table(name = "lx_order_pay", schema = "lx", catalog = "")
public class LxOrderPayEntity {
    private int payId;
    private long paySn;
    private int buyerId;

    @Id
    @Column(name = "pay_id")
    public int getPayId() {
        return payId;
    }

    public void setPayId(int payId) {
        this.payId = payId;
    }

    @Basic
    @Column(name = "pay_sn")
    public long getPaySn() {
        return paySn;
    }

    public void setPaySn(long paySn) {
        this.paySn = paySn;
    }

    @Basic
    @Column(name = "buyer_id")
    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LxOrderPayEntity that = (LxOrderPayEntity) o;

        if (payId != that.payId) return false;
        if (paySn != that.paySn) return false;
        if (buyerId != that.buyerId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = payId;
        result = 31 * result + (int) (paySn ^ (paySn >>> 32));
        result = 31 * result + buyerId;
        return result;
    }
}
