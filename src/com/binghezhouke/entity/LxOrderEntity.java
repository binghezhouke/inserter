package com.binghezhouke.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by binghezhouke on 16/7/8.
 */
@Entity
@Table(name = "lx_order", schema = "lx", catalog = "")
public class LxOrderEntity {
    private int orderId;
    private long orderSn;
    private long paySn;
    private int storeId;
    private String storeName;
    private int buyerId;
    private String buyerName;
    private String buyerEmail;
    private int addTime;
    private String paymentCode;
    private Integer paymentTime;
    private int finnshedTime;
    private BigDecimal goodsAmount;
    private BigDecimal orderAmount;
    private BigDecimal rcbAmount;
    private BigDecimal couponsAmount;
    private BigDecimal pdAmount;
    private BigDecimal shippingFee;
    private Byte evaluationState;
    private byte orderState;
    private Byte refundState;
    private Byte lockState;
    private byte deleteState;
    private BigDecimal refundAmount;
    private Integer delayTime;
    private byte orderFrom;
    private String shippingCode;
    private String unionId;
    private String euid;
    private String mid;
    private Integer clickTime;
    private String appUid;

    @Id
    @Column(name = "order_id")
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "order_sn")
    public long getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(long orderSn) {
        this.orderSn = orderSn;
    }

    @Basic
    @Column(name = "pay_sn")
    public long getPaySn() {
        return paySn;
    }

    public void setPaySn(long paySn) {
        this.paySn = paySn;
    }

    @Basic
    @Column(name = "store_id")
    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    @Basic
    @Column(name = "store_name")
    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    @Basic
    @Column(name = "buyer_id")
    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    @Basic
    @Column(name = "buyer_name")
    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    @Basic
    @Column(name = "buyer_email")
    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    @Basic
    @Column(name = "add_time")
    public int getAddTime() {
        return addTime;
    }

    public void setAddTime(int addTime) {
        this.addTime = addTime;
    }

    @Basic
    @Column(name = "payment_code")
    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    @Basic
    @Column(name = "payment_time")
    public Integer getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Integer paymentTime) {
        this.paymentTime = paymentTime;
    }

    @Basic
    @Column(name = "finnshed_time")
    public int getFinnshedTime() {
        return finnshedTime;
    }

    public void setFinnshedTime(int finnshedTime) {
        this.finnshedTime = finnshedTime;
    }

    @Basic
    @Column(name = "goods_amount")
    public BigDecimal getGoodsAmount() {
        return goodsAmount;
    }

    public void setGoodsAmount(BigDecimal goodsAmount) {
        this.goodsAmount = goodsAmount;
    }

    @Basic
    @Column(name = "order_amount")
    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    @Basic
    @Column(name = "rcb_amount")
    public BigDecimal getRcbAmount() {
        return rcbAmount;
    }

    public void setRcbAmount(BigDecimal rcbAmount) {
        this.rcbAmount = rcbAmount;
    }

    @Basic
    @Column(name = "coupons_amount")
    public BigDecimal getCouponsAmount() {
        return couponsAmount;
    }

    public void setCouponsAmount(BigDecimal couponsAmount) {
        this.couponsAmount = couponsAmount;
    }

    @Basic
    @Column(name = "pd_amount")
    public BigDecimal getPdAmount() {
        return pdAmount;
    }

    public void setPdAmount(BigDecimal pdAmount) {
        this.pdAmount = pdAmount;
    }

    @Basic
    @Column(name = "shipping_fee")
    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    @Basic
    @Column(name = "evaluation_state")
    public Byte getEvaluationState() {
        return evaluationState;
    }

    public void setEvaluationState(Byte evaluationState) {
        this.evaluationState = evaluationState;
    }

    @Basic
    @Column(name = "order_state")
    public byte getOrderState() {
        return orderState;
    }

    public void setOrderState(byte orderState) {
        this.orderState = orderState;
    }

    @Basic
    @Column(name = "refund_state")
    public Byte getRefundState() {
        return refundState;
    }

    public void setRefundState(Byte refundState) {
        this.refundState = refundState;
    }

    @Basic
    @Column(name = "lock_state")
    public Byte getLockState() {
        return lockState;
    }

    public void setLockState(Byte lockState) {
        this.lockState = lockState;
    }

    @Basic
    @Column(name = "delete_state")
    public byte getDeleteState() {
        return deleteState;
    }

    public void setDeleteState(byte deleteState) {
        this.deleteState = deleteState;
    }

    @Basic
    @Column(name = "refund_amount")
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    @Basic
    @Column(name = "delay_time")
    public Integer getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(Integer delayTime) {
        this.delayTime = delayTime;
    }

    @Basic
    @Column(name = "order_from")
    public byte getOrderFrom() {
        return orderFrom;
    }

    public void setOrderFrom(byte orderFrom) {
        this.orderFrom = orderFrom;
    }

    @Basic
    @Column(name = "shipping_code")
    public String getShippingCode() {
        return shippingCode;
    }

    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode;
    }

    @Basic
    @Column(name = "union_id")
    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    @Basic
    @Column(name = "euid")
    public String getEuid() {
        return euid;
    }

    public void setEuid(String euid) {
        this.euid = euid;
    }

    @Basic
    @Column(name = "mid")
    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    @Basic
    @Column(name = "click_time")
    public Integer getClickTime() {
        return clickTime;
    }

    public void setClickTime(Integer clickTime) {
        this.clickTime = clickTime;
    }

    @Basic
    @Column(name = "app_uid")
    public String getAppUid() {
        return appUid;
    }

    public void setAppUid(String appUid) {
        this.appUid = appUid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LxOrderEntity that = (LxOrderEntity) o;

        if (orderId != that.orderId) return false;
        if (orderSn != that.orderSn) return false;
        if (paySn != that.paySn) return false;
        if (storeId != that.storeId) return false;
        if (buyerId != that.buyerId) return false;
        if (addTime != that.addTime) return false;
        if (finnshedTime != that.finnshedTime) return false;
        if (orderState != that.orderState) return false;
        if (deleteState != that.deleteState) return false;
        if (orderFrom != that.orderFrom) return false;
        if (storeName != null ? !storeName.equals(that.storeName) : that.storeName != null) return false;
        if (buyerName != null ? !buyerName.equals(that.buyerName) : that.buyerName != null) return false;
        if (buyerEmail != null ? !buyerEmail.equals(that.buyerEmail) : that.buyerEmail != null) return false;
        if (paymentCode != null ? !paymentCode.equals(that.paymentCode) : that.paymentCode != null) return false;
        if (paymentTime != null ? !paymentTime.equals(that.paymentTime) : that.paymentTime != null) return false;
        if (goodsAmount != null ? !goodsAmount.equals(that.goodsAmount) : that.goodsAmount != null) return false;
        if (orderAmount != null ? !orderAmount.equals(that.orderAmount) : that.orderAmount != null) return false;
        if (rcbAmount != null ? !rcbAmount.equals(that.rcbAmount) : that.rcbAmount != null) return false;
        if (couponsAmount != null ? !couponsAmount.equals(that.couponsAmount) : that.couponsAmount != null)
            return false;
        if (pdAmount != null ? !pdAmount.equals(that.pdAmount) : that.pdAmount != null) return false;
        if (shippingFee != null ? !shippingFee.equals(that.shippingFee) : that.shippingFee != null) return false;
        if (evaluationState != null ? !evaluationState.equals(that.evaluationState) : that.evaluationState != null)
            return false;
        if (refundState != null ? !refundState.equals(that.refundState) : that.refundState != null) return false;
        if (lockState != null ? !lockState.equals(that.lockState) : that.lockState != null) return false;
        if (refundAmount != null ? !refundAmount.equals(that.refundAmount) : that.refundAmount != null) return false;
        if (delayTime != null ? !delayTime.equals(that.delayTime) : that.delayTime != null) return false;
        if (shippingCode != null ? !shippingCode.equals(that.shippingCode) : that.shippingCode != null) return false;
        if (unionId != null ? !unionId.equals(that.unionId) : that.unionId != null) return false;
        if (euid != null ? !euid.equals(that.euid) : that.euid != null) return false;
        if (mid != null ? !mid.equals(that.mid) : that.mid != null) return false;
        if (clickTime != null ? !clickTime.equals(that.clickTime) : that.clickTime != null) return false;
        if (appUid != null ? !appUid.equals(that.appUid) : that.appUid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderId;
        result = 31 * result + (int) (orderSn ^ (orderSn >>> 32));
        result = 31 * result + (int) (paySn ^ (paySn >>> 32));
        result = 31 * result + storeId;
        result = 31 * result + (storeName != null ? storeName.hashCode() : 0);
        result = 31 * result + buyerId;
        result = 31 * result + (buyerName != null ? buyerName.hashCode() : 0);
        result = 31 * result + (buyerEmail != null ? buyerEmail.hashCode() : 0);
        result = 31 * result + addTime;
        result = 31 * result + (paymentCode != null ? paymentCode.hashCode() : 0);
        result = 31 * result + (paymentTime != null ? paymentTime.hashCode() : 0);
        result = 31 * result + finnshedTime;
        result = 31 * result + (goodsAmount != null ? goodsAmount.hashCode() : 0);
        result = 31 * result + (orderAmount != null ? orderAmount.hashCode() : 0);
        result = 31 * result + (rcbAmount != null ? rcbAmount.hashCode() : 0);
        result = 31 * result + (couponsAmount != null ? couponsAmount.hashCode() : 0);
        result = 31 * result + (pdAmount != null ? pdAmount.hashCode() : 0);
        result = 31 * result + (shippingFee != null ? shippingFee.hashCode() : 0);
        result = 31 * result + (evaluationState != null ? evaluationState.hashCode() : 0);
        result = 31 * result + (int) orderState;
        result = 31 * result + (refundState != null ? refundState.hashCode() : 0);
        result = 31 * result + (lockState != null ? lockState.hashCode() : 0);
        result = 31 * result + (int) deleteState;
        result = 31 * result + (refundAmount != null ? refundAmount.hashCode() : 0);
        result = 31 * result + (delayTime != null ? delayTime.hashCode() : 0);
        result = 31 * result + (int) orderFrom;
        result = 31 * result + (shippingCode != null ? shippingCode.hashCode() : 0);
        result = 31 * result + (unionId != null ? unionId.hashCode() : 0);
        result = 31 * result + (euid != null ? euid.hashCode() : 0);
        result = 31 * result + (mid != null ? mid.hashCode() : 0);
        result = 31 * result + (clickTime != null ? clickTime.hashCode() : 0);
        result = 31 * result + (appUid != null ? appUid.hashCode() : 0);
        return result;
    }
}
