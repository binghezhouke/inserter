package com.binghezhouke.entity;

import javax.persistence.*;

/**
 * Created by binghezhouke on 16/7/8.
 */
@Entity
@Table(name = "lx_coupons", schema = "lx", catalog = "")
public class LxCouponsEntity {
    private int couponsId;
    private String couponsSn;
    private String couponsName;
    private byte couponsUsetimeType;
    private Integer couponsStart;
    private Integer couponsEndtime;
    private short couponsType;
    private int couponsLimit;
    private int couponsMoney;
    private int couponsNum;
    private short couponsUserLimit;
    private short couposEachOrderNum;
    private short couponsWay;
    private String couponsWayJson;
    private byte couponsSendtimeType;
    private Integer couponsSendStarttime;
    private Integer couponsSendEndtime;
    private short couponsSendLimittype;
    private String couponsSendLimitjson;
    private Short couponsStatus;
    private String couponsCreateuser;
    private int couponsAddtime;
    private int couponsUpdatetime;
    private String couponsUpdateuser;
    private String couponsCardId;
    private byte couponsCardStatus;

    @Id
    @Column(name = "coupons_id")
    public int getCouponsId() {
        return couponsId;
    }

    public void setCouponsId(int couponsId) {
        this.couponsId = couponsId;
    }

    @Basic
    @Column(name = "coupons_sn")
    public String getCouponsSn() {
        return couponsSn;
    }

    public void setCouponsSn(String couponsSn) {
        this.couponsSn = couponsSn;
    }

    @Basic
    @Column(name = "coupons_name")
    public String getCouponsName() {
        return couponsName;
    }

    public void setCouponsName(String couponsName) {
        this.couponsName = couponsName;
    }

    @Basic
    @Column(name = "coupons_usetime_type")
    public byte getCouponsUsetimeType() {
        return couponsUsetimeType;
    }

    public void setCouponsUsetimeType(byte couponsUsetimeType) {
        this.couponsUsetimeType = couponsUsetimeType;
    }

    @Basic
    @Column(name = "coupons_start")
    public Integer getCouponsStart() {
        return couponsStart;
    }

    public void setCouponsStart(Integer couponsStart) {
        this.couponsStart = couponsStart;
    }

    @Basic
    @Column(name = "coupons_endtime")
    public Integer getCouponsEndtime() {
        return couponsEndtime;
    }

    public void setCouponsEndtime(Integer couponsEndtime) {
        this.couponsEndtime = couponsEndtime;
    }

    @Basic
    @Column(name = "coupons_type")
    public short getCouponsType() {
        return couponsType;
    }

    public void setCouponsType(short couponsType) {
        this.couponsType = couponsType;
    }

    @Basic
    @Column(name = "coupons_limit")
    public int getCouponsLimit() {
        return couponsLimit;
    }

    public void setCouponsLimit(int couponsLimit) {
        this.couponsLimit = couponsLimit;
    }

    @Basic
    @Column(name = "coupons_money")
    public int getCouponsMoney() {
        return couponsMoney;
    }

    public void setCouponsMoney(int couponsMoney) {
        this.couponsMoney = couponsMoney;
    }

    @Basic
    @Column(name = "coupons_num")
    public int getCouponsNum() {
        return couponsNum;
    }

    public void setCouponsNum(int couponsNum) {
        this.couponsNum = couponsNum;
    }

    @Basic
    @Column(name = "coupons_user_limit")
    public short getCouponsUserLimit() {
        return couponsUserLimit;
    }

    public void setCouponsUserLimit(short couponsUserLimit) {
        this.couponsUserLimit = couponsUserLimit;
    }

    @Basic
    @Column(name = "coupos_each_order_num")
    public short getCouposEachOrderNum() {
        return couposEachOrderNum;
    }

    public void setCouposEachOrderNum(short couposEachOrderNum) {
        this.couposEachOrderNum = couposEachOrderNum;
    }

    @Basic
    @Column(name = "coupons_way")
    public short getCouponsWay() {
        return couponsWay;
    }

    public void setCouponsWay(short couponsWay) {
        this.couponsWay = couponsWay;
    }

    @Basic
    @Column(name = "coupons_way_json")
    public String getCouponsWayJson() {
        return couponsWayJson;
    }

    public void setCouponsWayJson(String couponsWayJson) {
        this.couponsWayJson = couponsWayJson;
    }

    @Basic
    @Column(name = "coupons_sendtime_type")
    public byte getCouponsSendtimeType() {
        return couponsSendtimeType;
    }

    public void setCouponsSendtimeType(byte couponsSendtimeType) {
        this.couponsSendtimeType = couponsSendtimeType;
    }

    @Basic
    @Column(name = "coupons_send_starttime")
    public Integer getCouponsSendStarttime() {
        return couponsSendStarttime;
    }

    public void setCouponsSendStarttime(Integer couponsSendStarttime) {
        this.couponsSendStarttime = couponsSendStarttime;
    }

    @Basic
    @Column(name = "coupons_send_endtime")
    public Integer getCouponsSendEndtime() {
        return couponsSendEndtime;
    }

    public void setCouponsSendEndtime(Integer couponsSendEndtime) {
        this.couponsSendEndtime = couponsSendEndtime;
    }

    @Basic
    @Column(name = "coupons_send_limittype")
    public short getCouponsSendLimittype() {
        return couponsSendLimittype;
    }

    public void setCouponsSendLimittype(short couponsSendLimittype) {
        this.couponsSendLimittype = couponsSendLimittype;
    }

    @Basic
    @Column(name = "coupons_send_limitjson")
    public String getCouponsSendLimitjson() {
        return couponsSendLimitjson;
    }

    public void setCouponsSendLimitjson(String couponsSendLimitjson) {
        this.couponsSendLimitjson = couponsSendLimitjson;
    }

    @Basic
    @Column(name = "coupons_status")
    public Short getCouponsStatus() {
        return couponsStatus;
    }

    public void setCouponsStatus(Short couponsStatus) {
        this.couponsStatus = couponsStatus;
    }

    @Basic
    @Column(name = "coupons_createuser")
    public String getCouponsCreateuser() {
        return couponsCreateuser;
    }

    public void setCouponsCreateuser(String couponsCreateuser) {
        this.couponsCreateuser = couponsCreateuser;
    }

    @Basic
    @Column(name = "coupons_addtime")
    public int getCouponsAddtime() {
        return couponsAddtime;
    }

    public void setCouponsAddtime(int couponsAddtime) {
        this.couponsAddtime = couponsAddtime;
    }

    @Basic
    @Column(name = "coupons_updatetime")
    public int getCouponsUpdatetime() {
        return couponsUpdatetime;
    }

    public void setCouponsUpdatetime(int couponsUpdatetime) {
        this.couponsUpdatetime = couponsUpdatetime;
    }

    @Basic
    @Column(name = "coupons_updateuser")
    public String getCouponsUpdateuser() {
        return couponsUpdateuser;
    }

    public void setCouponsUpdateuser(String couponsUpdateuser) {
        this.couponsUpdateuser = couponsUpdateuser;
    }

    @Basic
    @Column(name = "coupons_card_id")
    public String getCouponsCardId() {
        return couponsCardId;
    }

    public void setCouponsCardId(String couponsCardId) {
        this.couponsCardId = couponsCardId;
    }

    @Basic
    @Column(name = "coupons_card_status")
    public byte getCouponsCardStatus() {
        return couponsCardStatus;
    }

    public void setCouponsCardStatus(byte couponsCardStatus) {
        this.couponsCardStatus = couponsCardStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LxCouponsEntity that = (LxCouponsEntity) o;

        if (couponsId != that.couponsId) return false;
        if (couponsUsetimeType != that.couponsUsetimeType) return false;
        if (couponsType != that.couponsType) return false;
        if (couponsLimit != that.couponsLimit) return false;
        if (couponsMoney != that.couponsMoney) return false;
        if (couponsNum != that.couponsNum) return false;
        if (couponsUserLimit != that.couponsUserLimit) return false;
        if (couposEachOrderNum != that.couposEachOrderNum) return false;
        if (couponsWay != that.couponsWay) return false;
        if (couponsSendtimeType != that.couponsSendtimeType) return false;
        if (couponsSendLimittype != that.couponsSendLimittype) return false;
        if (couponsAddtime != that.couponsAddtime) return false;
        if (couponsUpdatetime != that.couponsUpdatetime) return false;
        if (couponsCardStatus != that.couponsCardStatus) return false;
        if (couponsSn != null ? !couponsSn.equals(that.couponsSn) : that.couponsSn != null) return false;
        if (couponsName != null ? !couponsName.equals(that.couponsName) : that.couponsName != null) return false;
        if (couponsStart != null ? !couponsStart.equals(that.couponsStart) : that.couponsStart != null) return false;
        if (couponsEndtime != null ? !couponsEndtime.equals(that.couponsEndtime) : that.couponsEndtime != null)
            return false;
        if (couponsWayJson != null ? !couponsWayJson.equals(that.couponsWayJson) : that.couponsWayJson != null)
            return false;
        if (couponsSendStarttime != null ? !couponsSendStarttime.equals(that.couponsSendStarttime) : that.couponsSendStarttime != null)
            return false;
        if (couponsSendEndtime != null ? !couponsSendEndtime.equals(that.couponsSendEndtime) : that.couponsSendEndtime != null)
            return false;
        if (couponsSendLimitjson != null ? !couponsSendLimitjson.equals(that.couponsSendLimitjson) : that.couponsSendLimitjson != null)
            return false;
        if (couponsStatus != null ? !couponsStatus.equals(that.couponsStatus) : that.couponsStatus != null)
            return false;
        if (couponsCreateuser != null ? !couponsCreateuser.equals(that.couponsCreateuser) : that.couponsCreateuser != null)
            return false;
        if (couponsUpdateuser != null ? !couponsUpdateuser.equals(that.couponsUpdateuser) : that.couponsUpdateuser != null)
            return false;
        if (couponsCardId != null ? !couponsCardId.equals(that.couponsCardId) : that.couponsCardId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = couponsId;
        result = 31 * result + (couponsSn != null ? couponsSn.hashCode() : 0);
        result = 31 * result + (couponsName != null ? couponsName.hashCode() : 0);
        result = 31 * result + (int) couponsUsetimeType;
        result = 31 * result + (couponsStart != null ? couponsStart.hashCode() : 0);
        result = 31 * result + (couponsEndtime != null ? couponsEndtime.hashCode() : 0);
        result = 31 * result + (int) couponsType;
        result = 31 * result + couponsLimit;
        result = 31 * result + couponsMoney;
        result = 31 * result + couponsNum;
        result = 31 * result + (int) couponsUserLimit;
        result = 31 * result + (int) couposEachOrderNum;
        result = 31 * result + (int) couponsWay;
        result = 31 * result + (couponsWayJson != null ? couponsWayJson.hashCode() : 0);
        result = 31 * result + (int) couponsSendtimeType;
        result = 31 * result + (couponsSendStarttime != null ? couponsSendStarttime.hashCode() : 0);
        result = 31 * result + (couponsSendEndtime != null ? couponsSendEndtime.hashCode() : 0);
        result = 31 * result + (int) couponsSendLimittype;
        result = 31 * result + (couponsSendLimitjson != null ? couponsSendLimitjson.hashCode() : 0);
        result = 31 * result + (couponsStatus != null ? couponsStatus.hashCode() : 0);
        result = 31 * result + (couponsCreateuser != null ? couponsCreateuser.hashCode() : 0);
        result = 31 * result + couponsAddtime;
        result = 31 * result + couponsUpdatetime;
        result = 31 * result + (couponsUpdateuser != null ? couponsUpdateuser.hashCode() : 0);
        result = 31 * result + (couponsCardId != null ? couponsCardId.hashCode() : 0);
        result = 31 * result + (int) couponsCardStatus;
        return result;
    }
}
