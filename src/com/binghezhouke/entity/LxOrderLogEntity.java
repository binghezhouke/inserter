package com.binghezhouke.entity;

import javax.persistence.*;

/**
 * Created by binghezhouke on 16/7/8.
 */
@Entity
@Table(name = "lx_order_log", schema = "lx", catalog = "")
public class LxOrderLogEntity {
    private int logId;
    private int orderId;
    private String logMsg;
    private int logTime;
    private String logRole;
    private String logUser;
    private String logOrderstate;

    @Id
    @Column(name = "log_id")
    public int getLogId() {
        return logId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }

    @Basic
    @Column(name = "order_id")
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "log_msg")
    public String getLogMsg() {
        return logMsg;
    }

    public void setLogMsg(String logMsg) {
        this.logMsg = logMsg;
    }

    @Basic
    @Column(name = "log_time")
    public int getLogTime() {
        return logTime;
    }

    public void setLogTime(int logTime) {
        this.logTime = logTime;
    }

    @Basic
    @Column(name = "log_role")
    public String getLogRole() {
        return logRole;
    }

    public void setLogRole(String logRole) {
        this.logRole = logRole;
    }

    @Basic
    @Column(name = "log_user")
    public String getLogUser() {
        return logUser;
    }

    public void setLogUser(String logUser) {
        this.logUser = logUser;
    }

    @Basic
    @Column(name = "log_orderstate")
    public String getLogOrderstate() {
        return logOrderstate;
    }

    public void setLogOrderstate(String logOrderstate) {
        this.logOrderstate = logOrderstate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LxOrderLogEntity entity = (LxOrderLogEntity) o;

        if (logId != entity.logId) return false;
        if (orderId != entity.orderId) return false;
        if (logTime != entity.logTime) return false;
        if (logMsg != null ? !logMsg.equals(entity.logMsg) : entity.logMsg != null) return false;
        if (logRole != null ? !logRole.equals(entity.logRole) : entity.logRole != null) return false;
        if (logUser != null ? !logUser.equals(entity.logUser) : entity.logUser != null) return false;
        if (logOrderstate != null ? !logOrderstate.equals(entity.logOrderstate) : entity.logOrderstate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = logId;
        result = 31 * result + orderId;
        result = 31 * result + (logMsg != null ? logMsg.hashCode() : 0);
        result = 31 * result + logTime;
        result = 31 * result + (logRole != null ? logRole.hashCode() : 0);
        result = 31 * result + (logUser != null ? logUser.hashCode() : 0);
        result = 31 * result + (logOrderstate != null ? logOrderstate.hashCode() : 0);
        return result;
    }
}
