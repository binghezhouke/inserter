package com.binghezhouke.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by binghezhouke on 16/7/8.
 */
@Entity
@Table(name = "lx_order_goods", schema = "lx", catalog = "")
public class LxOrderGoodsEntity {
    private int recId;
    private int orderId;
    private int goodsId;
    private String goodsName;
    private BigDecimal goodsPrice;
    private short goodsNum;
    private String goodsImage;
    private BigDecimal goodsPayPrice;
    private int storeId;
    private int buyerId;
    private short commisRate;
    private Double cpsCommis;
    private BigDecimal cpsPrice;

    @Id
    @Column(name = "rec_id")
    public int getRecId() {
        return recId;
    }

    public void setRecId(int recId) {
        this.recId = recId;
    }

    @Basic
    @Column(name = "order_id")
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "goods_id")
    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    @Basic
    @Column(name = "goods_name")
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @Basic
    @Column(name = "goods_price")
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    @Basic
    @Column(name = "goods_num")
    public short getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(short goodsNum) {
        this.goodsNum = goodsNum;
    }

    @Basic
    @Column(name = "goods_image")
    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    @Basic
    @Column(name = "goods_pay_price")
    public BigDecimal getGoodsPayPrice() {
        return goodsPayPrice;
    }

    public void setGoodsPayPrice(BigDecimal goodsPayPrice) {
        this.goodsPayPrice = goodsPayPrice;
    }

    @Basic
    @Column(name = "store_id")
    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    @Basic
    @Column(name = "buyer_id")
    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    @Basic
    @Column(name = "commis_rate")
    public short getCommisRate() {
        return commisRate;
    }

    public void setCommisRate(short commisRate) {
        this.commisRate = commisRate;
    }

    @Basic
    @Column(name = "cps_commis")
    public Double getCpsCommis() {
        return cpsCommis;
    }

    public void setCpsCommis(Double cpsCommis) {
        this.cpsCommis = cpsCommis;
    }

    @Basic
    @Column(name = "cps_price")
    public BigDecimal getCpsPrice() {
        return cpsPrice;
    }

    public void setCpsPrice(BigDecimal cpsPrice) {
        this.cpsPrice = cpsPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LxOrderGoodsEntity that = (LxOrderGoodsEntity) o;

        if (recId != that.recId) return false;
        if (orderId != that.orderId) return false;
        if (goodsId != that.goodsId) return false;
        if (goodsNum != that.goodsNum) return false;
        if (storeId != that.storeId) return false;
        if (buyerId != that.buyerId) return false;
        if (commisRate != that.commisRate) return false;
        if (goodsName != null ? !goodsName.equals(that.goodsName) : that.goodsName != null) return false;
        if (goodsPrice != null ? !goodsPrice.equals(that.goodsPrice) : that.goodsPrice != null) return false;
        if (goodsImage != null ? !goodsImage.equals(that.goodsImage) : that.goodsImage != null) return false;
        if (goodsPayPrice != null ? !goodsPayPrice.equals(that.goodsPayPrice) : that.goodsPayPrice != null)
            return false;
        if (cpsCommis != null ? !cpsCommis.equals(that.cpsCommis) : that.cpsCommis != null) return false;
        if (cpsPrice != null ? !cpsPrice.equals(that.cpsPrice) : that.cpsPrice != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recId;
        result = 31 * result + orderId;
        result = 31 * result + goodsId;
        result = 31 * result + (goodsName != null ? goodsName.hashCode() : 0);
        result = 31 * result + (goodsPrice != null ? goodsPrice.hashCode() : 0);
        result = 31 * result + (int) goodsNum;
        result = 31 * result + (goodsImage != null ? goodsImage.hashCode() : 0);
        result = 31 * result + (goodsPayPrice != null ? goodsPayPrice.hashCode() : 0);
        result = 31 * result + storeId;
        result = 31 * result + buyerId;
        result = 31 * result + (int) commisRate;
        result = 31 * result + (cpsCommis != null ? cpsCommis.hashCode() : 0);
        result = 31 * result + (cpsPrice != null ? cpsPrice.hashCode() : 0);
        return result;
    }
}
