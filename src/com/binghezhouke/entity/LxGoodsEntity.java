package com.binghezhouke.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by binghezhouke on 16/7/8.
 */
@Entity
@Table(name = "lx_goods", schema = "lx", catalog = "")
public class LxGoodsEntity {
    private int goodsId;
    private int goodsCommonid;
    private String goodsName;
    private String goodsJingle;
    private int storeId;
    private String storeName;
    private int gcId;
    private int gcId1;
    private int gcId2;
    private int gcId3;
    private int brandId;
    private BigDecimal goodsPrice;
    private BigDecimal goodsPromotionPrice;
    private byte goodsPromotionType;
    private BigDecimal goodsMarketprice;
    private String goodsSerial;
    private byte goodsStorageAlarm;
    private int goodsClick;
    private int goodsSalenum;
    private int goodsCollect;
    private String goodsSpec;
    private int goodsStorage;
    private String goodsImage;
    private byte goodsState;
    private byte goodsVerify;
    private int goodsAddtime;
    private int goodsEdittime;
    private int areaid1;
    private int areaid2;
    private int colorId;
    private BigDecimal goodsFreight;
    private byte goodsVat;
    private byte goodsCommend;
    private String goodsStcids;
    private byte evaluationGoodStar;
    private int evaluationCount;
    private byte isVirtual;
    private int virtualIndate;
    private byte virtualLimit;
    private byte virtualInvalidRefund;
    private byte isFcode;
    private byte isAppoint;
    private byte isPresell;
    private byte haveGift;
    private byte isOwnShop;
    private short userLimit;

    @Id
    @Column(name = "goods_id")
    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    @Basic
    @Column(name = "goods_commonid")
    public int getGoodsCommonid() {
        return goodsCommonid;
    }

    public void setGoodsCommonid(int goodsCommonid) {
        this.goodsCommonid = goodsCommonid;
    }

    @Basic
    @Column(name = "goods_name")
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @Basic
    @Column(name = "goods_jingle")
    public String getGoodsJingle() {
        return goodsJingle;
    }

    public void setGoodsJingle(String goodsJingle) {
        this.goodsJingle = goodsJingle;
    }

    @Basic
    @Column(name = "store_id")
    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    @Basic
    @Column(name = "store_name")
    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    @Basic
    @Column(name = "gc_id")
    public int getGcId() {
        return gcId;
    }

    public void setGcId(int gcId) {
        this.gcId = gcId;
    }

    @Basic
    @Column(name = "gc_id_1")
    public int getGcId1() {
        return gcId1;
    }

    public void setGcId1(int gcId1) {
        this.gcId1 = gcId1;
    }

    @Basic
    @Column(name = "gc_id_2")
    public int getGcId2() {
        return gcId2;
    }

    public void setGcId2(int gcId2) {
        this.gcId2 = gcId2;
    }

    @Basic
    @Column(name = "gc_id_3")
    public int getGcId3() {
        return gcId3;
    }

    public void setGcId3(int gcId3) {
        this.gcId3 = gcId3;
    }

    @Basic
    @Column(name = "brand_id")
    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    @Basic
    @Column(name = "goods_price")
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    @Basic
    @Column(name = "goods_promotion_price")
    public BigDecimal getGoodsPromotionPrice() {
        return goodsPromotionPrice;
    }

    public void setGoodsPromotionPrice(BigDecimal goodsPromotionPrice) {
        this.goodsPromotionPrice = goodsPromotionPrice;
    }

    @Basic
    @Column(name = "goods_promotion_type")
    public byte getGoodsPromotionType() {
        return goodsPromotionType;
    }

    public void setGoodsPromotionType(byte goodsPromotionType) {
        this.goodsPromotionType = goodsPromotionType;
    }

    @Basic
    @Column(name = "goods_marketprice")
    public BigDecimal getGoodsMarketprice() {
        return goodsMarketprice;
    }

    public void setGoodsMarketprice(BigDecimal goodsMarketprice) {
        this.goodsMarketprice = goodsMarketprice;
    }

    @Basic
    @Column(name = "goods_serial")
    public String getGoodsSerial() {
        return goodsSerial;
    }

    public void setGoodsSerial(String goodsSerial) {
        this.goodsSerial = goodsSerial;
    }

    @Basic
    @Column(name = "goods_storage_alarm")
    public byte getGoodsStorageAlarm() {
        return goodsStorageAlarm;
    }

    public void setGoodsStorageAlarm(byte goodsStorageAlarm) {
        this.goodsStorageAlarm = goodsStorageAlarm;
    }

    @Basic
    @Column(name = "goods_click")
    public int getGoodsClick() {
        return goodsClick;
    }

    public void setGoodsClick(int goodsClick) {
        this.goodsClick = goodsClick;
    }

    @Basic
    @Column(name = "goods_salenum")
    public int getGoodsSalenum() {
        return goodsSalenum;
    }

    public void setGoodsSalenum(int goodsSalenum) {
        this.goodsSalenum = goodsSalenum;
    }

    @Basic
    @Column(name = "goods_collect")
    public int getGoodsCollect() {
        return goodsCollect;
    }

    public void setGoodsCollect(int goodsCollect) {
        this.goodsCollect = goodsCollect;
    }

    @Basic
    @Column(name = "goods_spec")
    public String getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    @Basic
    @Column(name = "goods_storage")
    public int getGoodsStorage() {
        return goodsStorage;
    }

    public void setGoodsStorage(int goodsStorage) {
        this.goodsStorage = goodsStorage;
    }

    @Basic
    @Column(name = "goods_image")
    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    @Basic
    @Column(name = "goods_state")
    public byte getGoodsState() {
        return goodsState;
    }

    public void setGoodsState(byte goodsState) {
        this.goodsState = goodsState;
    }

    @Basic
    @Column(name = "goods_verify")
    public byte getGoodsVerify() {
        return goodsVerify;
    }

    public void setGoodsVerify(byte goodsVerify) {
        this.goodsVerify = goodsVerify;
    }

    @Basic
    @Column(name = "goods_addtime")
    public int getGoodsAddtime() {
        return goodsAddtime;
    }

    public void setGoodsAddtime(int goodsAddtime) {
        this.goodsAddtime = goodsAddtime;
    }

    @Basic
    @Column(name = "goods_edittime")
    public int getGoodsEdittime() {
        return goodsEdittime;
    }

    public void setGoodsEdittime(int goodsEdittime) {
        this.goodsEdittime = goodsEdittime;
    }

    @Basic
    @Column(name = "areaid_1")
    public int getAreaid1() {
        return areaid1;
    }

    public void setAreaid1(int areaid1) {
        this.areaid1 = areaid1;
    }

    @Basic
    @Column(name = "areaid_2")
    public int getAreaid2() {
        return areaid2;
    }

    public void setAreaid2(int areaid2) {
        this.areaid2 = areaid2;
    }

    @Basic
    @Column(name = "color_id")
    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    @Basic
    @Column(name = "goods_freight")
    public BigDecimal getGoodsFreight() {
        return goodsFreight;
    }

    public void setGoodsFreight(BigDecimal goodsFreight) {
        this.goodsFreight = goodsFreight;
    }

    @Basic
    @Column(name = "goods_vat")
    public byte getGoodsVat() {
        return goodsVat;
    }

    public void setGoodsVat(byte goodsVat) {
        this.goodsVat = goodsVat;
    }

    @Basic
    @Column(name = "goods_commend")
    public byte getGoodsCommend() {
        return goodsCommend;
    }

    public void setGoodsCommend(byte goodsCommend) {
        this.goodsCommend = goodsCommend;
    }

    @Basic
    @Column(name = "goods_stcids")
    public String getGoodsStcids() {
        return goodsStcids;
    }

    public void setGoodsStcids(String goodsStcids) {
        this.goodsStcids = goodsStcids;
    }

    @Basic
    @Column(name = "evaluation_good_star")
    public byte getEvaluationGoodStar() {
        return evaluationGoodStar;
    }

    public void setEvaluationGoodStar(byte evaluationGoodStar) {
        this.evaluationGoodStar = evaluationGoodStar;
    }

    @Basic
    @Column(name = "evaluation_count")
    public int getEvaluationCount() {
        return evaluationCount;
    }

    public void setEvaluationCount(int evaluationCount) {
        this.evaluationCount = evaluationCount;
    }

    @Basic
    @Column(name = "is_virtual")
    public byte getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(byte isVirtual) {
        this.isVirtual = isVirtual;
    }

    @Basic
    @Column(name = "virtual_indate")
    public int getVirtualIndate() {
        return virtualIndate;
    }

    public void setVirtualIndate(int virtualIndate) {
        this.virtualIndate = virtualIndate;
    }

    @Basic
    @Column(name = "virtual_limit")
    public byte getVirtualLimit() {
        return virtualLimit;
    }

    public void setVirtualLimit(byte virtualLimit) {
        this.virtualLimit = virtualLimit;
    }

    @Basic
    @Column(name = "virtual_invalid_refund")
    public byte getVirtualInvalidRefund() {
        return virtualInvalidRefund;
    }

    public void setVirtualInvalidRefund(byte virtualInvalidRefund) {
        this.virtualInvalidRefund = virtualInvalidRefund;
    }

    @Basic
    @Column(name = "is_fcode")
    public byte getIsFcode() {
        return isFcode;
    }

    public void setIsFcode(byte isFcode) {
        this.isFcode = isFcode;
    }

    @Basic
    @Column(name = "is_appoint")
    public byte getIsAppoint() {
        return isAppoint;
    }

    public void setIsAppoint(byte isAppoint) {
        this.isAppoint = isAppoint;
    }

    @Basic
    @Column(name = "is_presell")
    public byte getIsPresell() {
        return isPresell;
    }

    public void setIsPresell(byte isPresell) {
        this.isPresell = isPresell;
    }

    @Basic
    @Column(name = "have_gift")
    public byte getHaveGift() {
        return haveGift;
    }

    public void setHaveGift(byte haveGift) {
        this.haveGift = haveGift;
    }

    @Basic
    @Column(name = "is_own_shop")
    public byte getIsOwnShop() {
        return isOwnShop;
    }

    public void setIsOwnShop(byte isOwnShop) {
        this.isOwnShop = isOwnShop;
    }

    @Basic
    @Column(name = "user_limit")
    public short getUserLimit() {
        return userLimit;
    }

    public void setUserLimit(short userLimit) {
        this.userLimit = userLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LxGoodsEntity that = (LxGoodsEntity) o;

        if (goodsId != that.goodsId) return false;
        if (goodsCommonid != that.goodsCommonid) return false;
        if (storeId != that.storeId) return false;
        if (gcId != that.gcId) return false;
        if (gcId1 != that.gcId1) return false;
        if (gcId2 != that.gcId2) return false;
        if (gcId3 != that.gcId3) return false;
        if (brandId != that.brandId) return false;
        if (goodsPromotionType != that.goodsPromotionType) return false;
        if (goodsStorageAlarm != that.goodsStorageAlarm) return false;
        if (goodsClick != that.goodsClick) return false;
        if (goodsSalenum != that.goodsSalenum) return false;
        if (goodsCollect != that.goodsCollect) return false;
        if (goodsStorage != that.goodsStorage) return false;
        if (goodsState != that.goodsState) return false;
        if (goodsVerify != that.goodsVerify) return false;
        if (goodsAddtime != that.goodsAddtime) return false;
        if (goodsEdittime != that.goodsEdittime) return false;
        if (areaid1 != that.areaid1) return false;
        if (areaid2 != that.areaid2) return false;
        if (colorId != that.colorId) return false;
        if (goodsVat != that.goodsVat) return false;
        if (goodsCommend != that.goodsCommend) return false;
        if (evaluationGoodStar != that.evaluationGoodStar) return false;
        if (evaluationCount != that.evaluationCount) return false;
        if (isVirtual != that.isVirtual) return false;
        if (virtualIndate != that.virtualIndate) return false;
        if (virtualLimit != that.virtualLimit) return false;
        if (virtualInvalidRefund != that.virtualInvalidRefund) return false;
        if (isFcode != that.isFcode) return false;
        if (isAppoint != that.isAppoint) return false;
        if (isPresell != that.isPresell) return false;
        if (haveGift != that.haveGift) return false;
        if (isOwnShop != that.isOwnShop) return false;
        if (userLimit != that.userLimit) return false;
        if (goodsName != null ? !goodsName.equals(that.goodsName) : that.goodsName != null) return false;
        if (goodsJingle != null ? !goodsJingle.equals(that.goodsJingle) : that.goodsJingle != null) return false;
        if (storeName != null ? !storeName.equals(that.storeName) : that.storeName != null) return false;
        if (goodsPrice != null ? !goodsPrice.equals(that.goodsPrice) : that.goodsPrice != null) return false;
        if (goodsPromotionPrice != null ? !goodsPromotionPrice.equals(that.goodsPromotionPrice) : that.goodsPromotionPrice != null)
            return false;
        if (goodsMarketprice != null ? !goodsMarketprice.equals(that.goodsMarketprice) : that.goodsMarketprice != null)
            return false;
        if (goodsSerial != null ? !goodsSerial.equals(that.goodsSerial) : that.goodsSerial != null) return false;
        if (goodsSpec != null ? !goodsSpec.equals(that.goodsSpec) : that.goodsSpec != null) return false;
        if (goodsImage != null ? !goodsImage.equals(that.goodsImage) : that.goodsImage != null) return false;
        if (goodsFreight != null ? !goodsFreight.equals(that.goodsFreight) : that.goodsFreight != null) return false;
        if (goodsStcids != null ? !goodsStcids.equals(that.goodsStcids) : that.goodsStcids != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = goodsId;
        result = 31 * result + goodsCommonid;
        result = 31 * result + (goodsName != null ? goodsName.hashCode() : 0);
        result = 31 * result + (goodsJingle != null ? goodsJingle.hashCode() : 0);
        result = 31 * result + storeId;
        result = 31 * result + (storeName != null ? storeName.hashCode() : 0);
        result = 31 * result + gcId;
        result = 31 * result + gcId1;
        result = 31 * result + gcId2;
        result = 31 * result + gcId3;
        result = 31 * result + brandId;
        result = 31 * result + (goodsPrice != null ? goodsPrice.hashCode() : 0);
        result = 31 * result + (goodsPromotionPrice != null ? goodsPromotionPrice.hashCode() : 0);
        result = 31 * result + (int) goodsPromotionType;
        result = 31 * result + (goodsMarketprice != null ? goodsMarketprice.hashCode() : 0);
        result = 31 * result + (goodsSerial != null ? goodsSerial.hashCode() : 0);
        result = 31 * result + (int) goodsStorageAlarm;
        result = 31 * result + goodsClick;
        result = 31 * result + goodsSalenum;
        result = 31 * result + goodsCollect;
        result = 31 * result + (goodsSpec != null ? goodsSpec.hashCode() : 0);
        result = 31 * result + goodsStorage;
        result = 31 * result + (goodsImage != null ? goodsImage.hashCode() : 0);
        result = 31 * result + (int) goodsState;
        result = 31 * result + (int) goodsVerify;
        result = 31 * result + goodsAddtime;
        result = 31 * result + goodsEdittime;
        result = 31 * result + areaid1;
        result = 31 * result + areaid2;
        result = 31 * result + colorId;
        result = 31 * result + (goodsFreight != null ? goodsFreight.hashCode() : 0);
        result = 31 * result + (int) goodsVat;
        result = 31 * result + (int) goodsCommend;
        result = 31 * result + (goodsStcids != null ? goodsStcids.hashCode() : 0);
        result = 31 * result + (int) evaluationGoodStar;
        result = 31 * result + evaluationCount;
        result = 31 * result + (int) isVirtual;
        result = 31 * result + virtualIndate;
        result = 31 * result + (int) virtualLimit;
        result = 31 * result + (int) virtualInvalidRefund;
        result = 31 * result + (int) isFcode;
        result = 31 * result + (int) isAppoint;
        result = 31 * result + (int) isPresell;
        result = 31 * result + (int) haveGift;
        result = 31 * result + (int) isOwnShop;
        result = 31 * result + (int) userLimit;
        return result;
    }
}
