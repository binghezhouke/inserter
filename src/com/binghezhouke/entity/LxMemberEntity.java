package com.binghezhouke.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by binghezhouke on 16/7/8.
 */
@Entity
@Table(name = "lx_member", schema = "lx", catalog = "")
public class LxMemberEntity {
    private int memberId;
    private String memberName;
    private String memberTruename;
    private String memberAvatar;
    private Byte memberSex;
    private Date memberBirthday;
    private String memberPasswd;
    private String memberPaypwd;
    private String memberEmail;
    private byte memberEmailBind;
    private String memberMobile;
    private byte memberMobileBind;
    private String memberQq;
    private String memberWw;
    private int memberLoginNum;
    private String memberTime;
    private String memberLoginTime;
    private String memberOldLoginTime;
    private String memberLoginIp;
    private String memberOldLoginIp;
    private String memberQqopenid;
    private String memberQqinfo;
    private String memberSinaopenid;
    private String memberSinainfo;
    private int memberPoints;
    private BigDecimal availablePredeposit;
    private BigDecimal freezePredeposit;
    private BigDecimal availableRcBalance;
    private BigDecimal freezeRcBalance;
    private byte informAllow;
    private byte isBuy;
    private byte isAllowtalk;
    private byte memberState;
    private int memberSnsvisitnum;
    private Integer memberAreaid;
    private Integer memberCityid;
    private Integer memberProvinceid;
    private String memberAreainfo;
    private String memberPrivacy;
    private String memberQuicklink;
    private int memberExppoints;
    private Byte isEmailVerify;
    private Integer inviteTime;
    private String invitecode;
    private Integer memberQdTime;
    private byte memberPrizes;

    @Id
    @Column(name = "member_id")
    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    @Basic
    @Column(name = "member_name")
    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    @Basic
    @Column(name = "member_truename")
    public String getMemberTruename() {
        return memberTruename;
    }

    public void setMemberTruename(String memberTruename) {
        this.memberTruename = memberTruename;
    }

    @Basic
    @Column(name = "member_avatar")
    public String getMemberAvatar() {
        return memberAvatar;
    }

    public void setMemberAvatar(String memberAvatar) {
        this.memberAvatar = memberAvatar;
    }

    @Basic
    @Column(name = "member_sex")
    public Byte getMemberSex() {
        return memberSex;
    }

    public void setMemberSex(Byte memberSex) {
        this.memberSex = memberSex;
    }

    @Basic
    @Column(name = "member_birthday")
    public Date getMemberBirthday() {
        return memberBirthday;
    }

    public void setMemberBirthday(Date memberBirthday) {
        this.memberBirthday = memberBirthday;
    }

    @Basic
    @Column(name = "member_passwd")
    public String getMemberPasswd() {
        return memberPasswd;
    }

    public void setMemberPasswd(String memberPasswd) {
        this.memberPasswd = memberPasswd;
    }

    @Basic
    @Column(name = "member_paypwd")
    public String getMemberPaypwd() {
        return memberPaypwd;
    }

    public void setMemberPaypwd(String memberPaypwd) {
        this.memberPaypwd = memberPaypwd;
    }

    @Basic
    @Column(name = "member_email")
    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    @Basic
    @Column(name = "member_email_bind")
    public byte getMemberEmailBind() {
        return memberEmailBind;
    }

    public void setMemberEmailBind(byte memberEmailBind) {
        this.memberEmailBind = memberEmailBind;
    }

    @Basic
    @Column(name = "member_mobile")
    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    @Basic
    @Column(name = "member_mobile_bind")
    public byte getMemberMobileBind() {
        return memberMobileBind;
    }

    public void setMemberMobileBind(byte memberMobileBind) {
        this.memberMobileBind = memberMobileBind;
    }

    @Basic
    @Column(name = "member_qq")
    public String getMemberQq() {
        return memberQq;
    }

    public void setMemberQq(String memberQq) {
        this.memberQq = memberQq;
    }

    @Basic
    @Column(name = "member_ww")
    public String getMemberWw() {
        return memberWw;
    }

    public void setMemberWw(String memberWw) {
        this.memberWw = memberWw;
    }

    @Basic
    @Column(name = "member_login_num")
    public int getMemberLoginNum() {
        return memberLoginNum;
    }

    public void setMemberLoginNum(int memberLoginNum) {
        this.memberLoginNum = memberLoginNum;
    }

    @Basic
    @Column(name = "member_time")
    public String getMemberTime() {
        return memberTime;
    }

    public void setMemberTime(String memberTime) {
        this.memberTime = memberTime;
    }

    @Basic
    @Column(name = "member_login_time")
    public String getMemberLoginTime() {
        return memberLoginTime;
    }

    public void setMemberLoginTime(String memberLoginTime) {
        this.memberLoginTime = memberLoginTime;
    }

    @Basic
    @Column(name = "member_old_login_time")
    public String getMemberOldLoginTime() {
        return memberOldLoginTime;
    }

    public void setMemberOldLoginTime(String memberOldLoginTime) {
        this.memberOldLoginTime = memberOldLoginTime;
    }

    @Basic
    @Column(name = "member_login_ip")
    public String getMemberLoginIp() {
        return memberLoginIp;
    }

    public void setMemberLoginIp(String memberLoginIp) {
        this.memberLoginIp = memberLoginIp;
    }

    @Basic
    @Column(name = "member_old_login_ip")
    public String getMemberOldLoginIp() {
        return memberOldLoginIp;
    }

    public void setMemberOldLoginIp(String memberOldLoginIp) {
        this.memberOldLoginIp = memberOldLoginIp;
    }

    @Basic
    @Column(name = "member_qqopenid")
    public String getMemberQqopenid() {
        return memberQqopenid;
    }

    public void setMemberQqopenid(String memberQqopenid) {
        this.memberQqopenid = memberQqopenid;
    }

    @Basic
    @Column(name = "member_qqinfo")
    public String getMemberQqinfo() {
        return memberQqinfo;
    }

    public void setMemberQqinfo(String memberQqinfo) {
        this.memberQqinfo = memberQqinfo;
    }

    @Basic
    @Column(name = "member_sinaopenid")
    public String getMemberSinaopenid() {
        return memberSinaopenid;
    }

    public void setMemberSinaopenid(String memberSinaopenid) {
        this.memberSinaopenid = memberSinaopenid;
    }

    @Basic
    @Column(name = "member_sinainfo")
    public String getMemberSinainfo() {
        return memberSinainfo;
    }

    public void setMemberSinainfo(String memberSinainfo) {
        this.memberSinainfo = memberSinainfo;
    }

    @Basic
    @Column(name = "member_points")
    public int getMemberPoints() {
        return memberPoints;
    }

    public void setMemberPoints(int memberPoints) {
        this.memberPoints = memberPoints;
    }

    @Basic
    @Column(name = "available_predeposit")
    public BigDecimal getAvailablePredeposit() {
        return availablePredeposit;
    }

    public void setAvailablePredeposit(BigDecimal availablePredeposit) {
        this.availablePredeposit = availablePredeposit;
    }

    @Basic
    @Column(name = "freeze_predeposit")
    public BigDecimal getFreezePredeposit() {
        return freezePredeposit;
    }

    public void setFreezePredeposit(BigDecimal freezePredeposit) {
        this.freezePredeposit = freezePredeposit;
    }

    @Basic
    @Column(name = "available_rc_balance")
    public BigDecimal getAvailableRcBalance() {
        return availableRcBalance;
    }

    public void setAvailableRcBalance(BigDecimal availableRcBalance) {
        this.availableRcBalance = availableRcBalance;
    }

    @Basic
    @Column(name = "freeze_rc_balance")
    public BigDecimal getFreezeRcBalance() {
        return freezeRcBalance;
    }

    public void setFreezeRcBalance(BigDecimal freezeRcBalance) {
        this.freezeRcBalance = freezeRcBalance;
    }

    @Basic
    @Column(name = "inform_allow")
    public byte getInformAllow() {
        return informAllow;
    }

    public void setInformAllow(byte informAllow) {
        this.informAllow = informAllow;
    }

    @Basic
    @Column(name = "is_buy")
    public byte getIsBuy() {
        return isBuy;
    }

    public void setIsBuy(byte isBuy) {
        this.isBuy = isBuy;
    }

    @Basic
    @Column(name = "is_allowtalk")
    public byte getIsAllowtalk() {
        return isAllowtalk;
    }

    public void setIsAllowtalk(byte isAllowtalk) {
        this.isAllowtalk = isAllowtalk;
    }

    @Basic
    @Column(name = "member_state")
    public byte getMemberState() {
        return memberState;
    }

    public void setMemberState(byte memberState) {
        this.memberState = memberState;
    }

    @Basic
    @Column(name = "member_snsvisitnum")
    public int getMemberSnsvisitnum() {
        return memberSnsvisitnum;
    }

    public void setMemberSnsvisitnum(int memberSnsvisitnum) {
        this.memberSnsvisitnum = memberSnsvisitnum;
    }

    @Basic
    @Column(name = "member_areaid")
    public Integer getMemberAreaid() {
        return memberAreaid;
    }

    public void setMemberAreaid(Integer memberAreaid) {
        this.memberAreaid = memberAreaid;
    }

    @Basic
    @Column(name = "member_cityid")
    public Integer getMemberCityid() {
        return memberCityid;
    }

    public void setMemberCityid(Integer memberCityid) {
        this.memberCityid = memberCityid;
    }

    @Basic
    @Column(name = "member_provinceid")
    public Integer getMemberProvinceid() {
        return memberProvinceid;
    }

    public void setMemberProvinceid(Integer memberProvinceid) {
        this.memberProvinceid = memberProvinceid;
    }

    @Basic
    @Column(name = "member_areainfo")
    public String getMemberAreainfo() {
        return memberAreainfo;
    }

    public void setMemberAreainfo(String memberAreainfo) {
        this.memberAreainfo = memberAreainfo;
    }

    @Basic
    @Column(name = "member_privacy")
    public String getMemberPrivacy() {
        return memberPrivacy;
    }

    public void setMemberPrivacy(String memberPrivacy) {
        this.memberPrivacy = memberPrivacy;
    }

    @Basic
    @Column(name = "member_quicklink")
    public String getMemberQuicklink() {
        return memberQuicklink;
    }

    public void setMemberQuicklink(String memberQuicklink) {
        this.memberQuicklink = memberQuicklink;
    }

    @Basic
    @Column(name = "member_exppoints")
    public int getMemberExppoints() {
        return memberExppoints;
    }

    public void setMemberExppoints(int memberExppoints) {
        this.memberExppoints = memberExppoints;
    }

    @Basic
    @Column(name = "is_email_verify")
    public Byte getIsEmailVerify() {
        return isEmailVerify;
    }

    public void setIsEmailVerify(Byte isEmailVerify) {
        this.isEmailVerify = isEmailVerify;
    }

    @Basic
    @Column(name = "invite_time")
    public Integer getInviteTime() {
        return inviteTime;
    }

    public void setInviteTime(Integer inviteTime) {
        this.inviteTime = inviteTime;
    }

    @Basic
    @Column(name = "invitecode")
    public String getInvitecode() {
        return invitecode;
    }

    public void setInvitecode(String invitecode) {
        this.invitecode = invitecode;
    }

    @Basic
    @Column(name = "member_qd_time")
    public Integer getMemberQdTime() {
        return memberQdTime;
    }

    public void setMemberQdTime(Integer memberQdTime) {
        this.memberQdTime = memberQdTime;
    }

    @Basic
    @Column(name = "member_prizes")
    public byte getMemberPrizes() {
        return memberPrizes;
    }

    public void setMemberPrizes(byte memberPrizes) {
        this.memberPrizes = memberPrizes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LxMemberEntity that = (LxMemberEntity) o;

        if (memberId != that.memberId) return false;
        if (memberEmailBind != that.memberEmailBind) return false;
        if (memberMobileBind != that.memberMobileBind) return false;
        if (memberLoginNum != that.memberLoginNum) return false;
        if (memberPoints != that.memberPoints) return false;
        if (informAllow != that.informAllow) return false;
        if (isBuy != that.isBuy) return false;
        if (isAllowtalk != that.isAllowtalk) return false;
        if (memberState != that.memberState) return false;
        if (memberSnsvisitnum != that.memberSnsvisitnum) return false;
        if (memberExppoints != that.memberExppoints) return false;
        if (memberPrizes != that.memberPrizes) return false;
        if (memberName != null ? !memberName.equals(that.memberName) : that.memberName != null) return false;
        if (memberTruename != null ? !memberTruename.equals(that.memberTruename) : that.memberTruename != null)
            return false;
        if (memberAvatar != null ? !memberAvatar.equals(that.memberAvatar) : that.memberAvatar != null) return false;
        if (memberSex != null ? !memberSex.equals(that.memberSex) : that.memberSex != null) return false;
        if (memberBirthday != null ? !memberBirthday.equals(that.memberBirthday) : that.memberBirthday != null)
            return false;
        if (memberPasswd != null ? !memberPasswd.equals(that.memberPasswd) : that.memberPasswd != null) return false;
        if (memberPaypwd != null ? !memberPaypwd.equals(that.memberPaypwd) : that.memberPaypwd != null) return false;
        if (memberEmail != null ? !memberEmail.equals(that.memberEmail) : that.memberEmail != null) return false;
        if (memberMobile != null ? !memberMobile.equals(that.memberMobile) : that.memberMobile != null) return false;
        if (memberQq != null ? !memberQq.equals(that.memberQq) : that.memberQq != null) return false;
        if (memberWw != null ? !memberWw.equals(that.memberWw) : that.memberWw != null) return false;
        if (memberTime != null ? !memberTime.equals(that.memberTime) : that.memberTime != null) return false;
        if (memberLoginTime != null ? !memberLoginTime.equals(that.memberLoginTime) : that.memberLoginTime != null)
            return false;
        if (memberOldLoginTime != null ? !memberOldLoginTime.equals(that.memberOldLoginTime) : that.memberOldLoginTime != null)
            return false;
        if (memberLoginIp != null ? !memberLoginIp.equals(that.memberLoginIp) : that.memberLoginIp != null)
            return false;
        if (memberOldLoginIp != null ? !memberOldLoginIp.equals(that.memberOldLoginIp) : that.memberOldLoginIp != null)
            return false;
        if (memberQqopenid != null ? !memberQqopenid.equals(that.memberQqopenid) : that.memberQqopenid != null)
            return false;
        if (memberQqinfo != null ? !memberQqinfo.equals(that.memberQqinfo) : that.memberQqinfo != null) return false;
        if (memberSinaopenid != null ? !memberSinaopenid.equals(that.memberSinaopenid) : that.memberSinaopenid != null)
            return false;
        if (memberSinainfo != null ? !memberSinainfo.equals(that.memberSinainfo) : that.memberSinainfo != null)
            return false;
        if (availablePredeposit != null ? !availablePredeposit.equals(that.availablePredeposit) : that.availablePredeposit != null)
            return false;
        if (freezePredeposit != null ? !freezePredeposit.equals(that.freezePredeposit) : that.freezePredeposit != null)
            return false;
        if (availableRcBalance != null ? !availableRcBalance.equals(that.availableRcBalance) : that.availableRcBalance != null)
            return false;
        if (freezeRcBalance != null ? !freezeRcBalance.equals(that.freezeRcBalance) : that.freezeRcBalance != null)
            return false;
        if (memberAreaid != null ? !memberAreaid.equals(that.memberAreaid) : that.memberAreaid != null) return false;
        if (memberCityid != null ? !memberCityid.equals(that.memberCityid) : that.memberCityid != null) return false;
        if (memberProvinceid != null ? !memberProvinceid.equals(that.memberProvinceid) : that.memberProvinceid != null)
            return false;
        if (memberAreainfo != null ? !memberAreainfo.equals(that.memberAreainfo) : that.memberAreainfo != null)
            return false;
        if (memberPrivacy != null ? !memberPrivacy.equals(that.memberPrivacy) : that.memberPrivacy != null)
            return false;
        if (memberQuicklink != null ? !memberQuicklink.equals(that.memberQuicklink) : that.memberQuicklink != null)
            return false;
        if (isEmailVerify != null ? !isEmailVerify.equals(that.isEmailVerify) : that.isEmailVerify != null)
            return false;
        if (inviteTime != null ? !inviteTime.equals(that.inviteTime) : that.inviteTime != null) return false;
        if (invitecode != null ? !invitecode.equals(that.invitecode) : that.invitecode != null) return false;
        if (memberQdTime != null ? !memberQdTime.equals(that.memberQdTime) : that.memberQdTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = memberId;
        result = 31 * result + (memberName != null ? memberName.hashCode() : 0);
        result = 31 * result + (memberTruename != null ? memberTruename.hashCode() : 0);
        result = 31 * result + (memberAvatar != null ? memberAvatar.hashCode() : 0);
        result = 31 * result + (memberSex != null ? memberSex.hashCode() : 0);
        result = 31 * result + (memberBirthday != null ? memberBirthday.hashCode() : 0);
        result = 31 * result + (memberPasswd != null ? memberPasswd.hashCode() : 0);
        result = 31 * result + (memberPaypwd != null ? memberPaypwd.hashCode() : 0);
        result = 31 * result + (memberEmail != null ? memberEmail.hashCode() : 0);
        result = 31 * result + (int) memberEmailBind;
        result = 31 * result + (memberMobile != null ? memberMobile.hashCode() : 0);
        result = 31 * result + (int) memberMobileBind;
        result = 31 * result + (memberQq != null ? memberQq.hashCode() : 0);
        result = 31 * result + (memberWw != null ? memberWw.hashCode() : 0);
        result = 31 * result + memberLoginNum;
        result = 31 * result + (memberTime != null ? memberTime.hashCode() : 0);
        result = 31 * result + (memberLoginTime != null ? memberLoginTime.hashCode() : 0);
        result = 31 * result + (memberOldLoginTime != null ? memberOldLoginTime.hashCode() : 0);
        result = 31 * result + (memberLoginIp != null ? memberLoginIp.hashCode() : 0);
        result = 31 * result + (memberOldLoginIp != null ? memberOldLoginIp.hashCode() : 0);
        result = 31 * result + (memberQqopenid != null ? memberQqopenid.hashCode() : 0);
        result = 31 * result + (memberQqinfo != null ? memberQqinfo.hashCode() : 0);
        result = 31 * result + (memberSinaopenid != null ? memberSinaopenid.hashCode() : 0);
        result = 31 * result + (memberSinainfo != null ? memberSinainfo.hashCode() : 0);
        result = 31 * result + memberPoints;
        result = 31 * result + (availablePredeposit != null ? availablePredeposit.hashCode() : 0);
        result = 31 * result + (freezePredeposit != null ? freezePredeposit.hashCode() : 0);
        result = 31 * result + (availableRcBalance != null ? availableRcBalance.hashCode() : 0);
        result = 31 * result + (freezeRcBalance != null ? freezeRcBalance.hashCode() : 0);
        result = 31 * result + (int) informAllow;
        result = 31 * result + (int) isBuy;
        result = 31 * result + (int) isAllowtalk;
        result = 31 * result + (int) memberState;
        result = 31 * result + memberSnsvisitnum;
        result = 31 * result + (memberAreaid != null ? memberAreaid.hashCode() : 0);
        result = 31 * result + (memberCityid != null ? memberCityid.hashCode() : 0);
        result = 31 * result + (memberProvinceid != null ? memberProvinceid.hashCode() : 0);
        result = 31 * result + (memberAreainfo != null ? memberAreainfo.hashCode() : 0);
        result = 31 * result + (memberPrivacy != null ? memberPrivacy.hashCode() : 0);
        result = 31 * result + (memberQuicklink != null ? memberQuicklink.hashCode() : 0);
        result = 31 * result + memberExppoints;
        result = 31 * result + (isEmailVerify != null ? isEmailVerify.hashCode() : 0);
        result = 31 * result + (inviteTime != null ? inviteTime.hashCode() : 0);
        result = 31 * result + (invitecode != null ? invitecode.hashCode() : 0);
        result = 31 * result + (memberQdTime != null ? memberQdTime.hashCode() : 0);
        result = 31 * result + (int) memberPrizes;
        return result;
    }
}
