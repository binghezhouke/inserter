package com.binghezhouk.main;

import com.binghezhouk.main.OrmHelper.OrderAdder;
import com.binghezhouk.main.OrmHelper.UserAdder;
import com.binghezhouke.entity.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by binghezhouke on 16/7/8.
 */
public class Main {
//    private static final String nameFile ="/Users/binghezhouke/IdeaProjects/hibernate/res/name.csv";
//    private static final String emailFile ="/Users/binghezhouke/IdeaProjects/hibernate/res/email.csv";

//    private static Integer[] globalGoodsIds=new Integer[]{11545,11544,11543,11542,11524,11520,11516,11507,11506,11493,11489,11488,11485,11484,11481,11479,11474,11470,11466,11464,11462,11461,11410,11399,11395,11386,11381,11376,11375,11370,11338,11329,11322,11320,11317,16759,16363,15078,14630,14621,14615,14614,14613,14609,14601,14600,14599,14598,14597,14596,14595,14593,14592,14590,14586,14585,14583,14582,14581,14580,14579,13649,13642,13630,13629,13627,13105,13103,13100,13096,11754,11753,11752,11751,11750,11749,11748,12059,12050,12043,12035,12021,12011,11571,11570,13362,13361,12692,12587,12586,12585,12584,12581,12580,12579,12576,12572,12570,12566,12565,12564,12563,12562,12561,12560,12559,12558,12557,12556,14233,14232,14231,14230,14229,14228,14227,14226,14225,14224,14223,14222,16312,16311,16117,16113,16108,16077,16076,16073,};
    public static void main(String[] args) {
        System.out.println("len:" +args.length);
        if (args.length!=3){
            System.out.println("use: java -jar *.java nameFile emailFile excelFile");
            System.exit(1);
        }
        String nameFile = args[0];
        String emailFile = args[1];
        String excelFile = args[2];
        Configuration configuration = new Configuration().configure("session.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
//        Session session = sessionFactory.openSession();
//        Query query = session.createQuery("from LxGoodsEntity E where E.goodsId in (:IDs)").setMaxResults(1000);
//        query.setParameterList("IDs", globalGoodsIds);
//        List<LxGoodsEntity> goodsList = query.list();
//
//
//        query = session.createQuery("from LxMemberEntity ").setMaxResults(1000);
//
//        getLastOrdernSn(sessionFactory);
////        System.out.println(lastOrderId);
//        List<LxMemberEntity> memberList = query.list();
//        session.close();
////        for (int i = 0; i < 10; i++) {
////            System.out.println(memberList.get(i).getMemberName());
////        }
////
////        insertOrder(sessionFactory, memberList.get(0),goodsList.get(0), (int)(System.currentTimeMillis()/1000));
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//
//        try {
//            Date date = (simpleDateFormat.parse("2016-07-09"));
////            long mTime = date.getTime();
//            long mTime = date.getTime() - (long) (1000L * 3600L * 24L * 61L);
//            for (int i = 0; i < 62; i++) {
//                date = new Date(mTime);
//                System.out.println(date);
//                handleOneDay(sessionFactory, date, goodsList, memberList);
//                System.out.println();
//                mTime = date.getTime() + (3600L * 1000L * 24L);
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        UserAdder adder = new UserAdder(sessionFactory, nameFile,emailFile,0);
//        long start = (System.currentTimeMillis());
//        adder.insertOneDayUser(sessionFactory, 10000, Util.strToDate("2017-08-8"));
//        System.out.println(System.currentTimeMillis()-start);
//
        OrderAdder orderAdder = new OrderAdder(sessionFactory);
//        start = System.currentTimeMillis();
//        orderAdder.handleOneDay(1000, Util.strToDate("2017-08-09"));
//        System.out.println(System.currentTimeMillis()-start);

            List<String> records = Util.readFile(excelFile);
        int idx = 0;
        for (String tmp:records){
            String[] one = tmp.split(",");
            System.out.println(String.format("%d/%d:%s", idx++,records.size(),tmp ));
            String date = one[0];
            int userCount = Integer.parseInt(one[1]);
            int orderCount = Integer.parseInt(one[2]);
            adder.insertOneDayUser(sessionFactory, userCount, Util.strToDate(date));;
            orderAdder.handleOneDay(orderCount, Util.strToDate(date));

        }
    }

}
