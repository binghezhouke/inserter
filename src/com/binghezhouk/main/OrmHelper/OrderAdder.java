package com.binghezhouk.main.OrmHelper;

import com.binghezhouk.main.Util;
import com.binghezhouke.entity.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by binghezhouke on 16/7/10.
 */
public class OrderAdder {
    private long lastOrderSN = -1;
    private long lastPaySn = -1;
    private SessionFactory mSessionFactory;
    private List<LxGoodsEntity> goodsEntityList = null;
    private static Integer[] globalGoodsIds=new Integer[]{11545,11544,11543,11542,11524,11520,11516,11507,11506,11493,11489,11488,11485,11484,11481,11479,11474,11470,11466,11464,11462,11461,11410,11399,11395,11386,11381,11376,11375,11370,11338,11329,11322,11320,11317,16759,16363,15078,14630,14621,14615,14614,14613,14609,14601,14600,14599,14598,14597,14596,14595,14593,14592,14590,14586,14585,14583,14582,14581,14580,14579,13649,13642,13630,13629,13627,13105,13103,13100,13096,11754,11753,11752,11751,11750,11749,11748,12059,12050,12043,12035,12021,12011,11571,11570,13362,13361,12692,12587,12586,12585,12584,12581,12580,12579,12576,12572,12570,12566,12565,12564,12563,12562,12561,12560,12559,12558,12557,12556,14233,14232,14231,14230,14229,14228,14227,14226,14225,14224,14223,14222,16312,16311,16117,16113,16108,16077,16076,16073,};

    public OrderAdder(SessionFactory sessionFactory) {
        mSessionFactory = sessionFactory;

        LxOrderEntity lxOrderEntity = getLastOrderEntity(sessionFactory);
        lastPaySn = lxOrderEntity.getOrderSn();

        lastOrderSN = getLastOrderSN(sessionFactory);


        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from LxGoodsEntity E where E.goodsId in (:IDs)").setMaxResults(1000);
        query.setParameterList("IDs", globalGoodsIds);
        goodsEntityList = query.list();
        session.close();
    }

    // do insert
    private void insertOrderLog(LxOrderEntity orderEntity) {
        LxOrderLogEntity entity = new LxOrderLogEntity();
        entity.setOrderId(orderEntity.getOrderId());
        entity.setLogMsg("已收货");
        entity.setLogTime(orderEntity.getPaymentTime() + 1000);
        entity.setLogRole("系统");
        entity.setLogUser("系统");
        entity.setLogOrderstate("40");

        Session session = mSessionFactory.openSession();
        session.save(entity);
        session.flush();
        session.close();
    }

    private static LxOrderEntity getLastOrderEntity(SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from LxOrderEntity E " + "ORDER BY E.orderId desc").setMaxResults(1);
        LxOrderEntity orderEntity = (LxOrderEntity) query.list().get(0);
        session.close();
        return orderEntity;
    }
    private static long getLastOrderSN(SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from LxOrderEntity E " + "ORDER BY E.orderSn desc").setMaxResults(1);
        LxOrderEntity orderEntity = (LxOrderEntity) query.list().get(0);
        session.close();
        return orderEntity.getOrderSn();
    }


    private static void insertOrderGoods(SessionFactory sessionFactory, LxMemberEntity memberEntity, LxGoodsEntity goodsEntity,
                                         LxOrderEntity orderEntity, int goodsNum) {
        LxOrderGoodsEntity entity = new LxOrderGoodsEntity();

        entity.setOrderId(orderEntity.getOrderId());

        entity.setGoodsId(goodsEntity.getGoodsId());
        entity.setGoodsName(goodsEntity.getGoodsName());
        entity.setGoodsImage(goodsEntity.getGoodsImage());
        entity.setGoodsPayPrice(goodsEntity.getGoodsPromotionPrice());
        entity.setGoodsPrice(goodsEntity.getGoodsPrice());
        entity.setStoreId(goodsEntity.getStoreId());

        entity.setGoodsNum((short) goodsNum);

        entity.setBuyerId(memberEntity.getMemberId());

        Session session = sessionFactory.openSession();
        session.save(entity);
        session.flush();
        session.close();
    }

    private void insertOrderPay(SessionFactory sessionFactory, LxOrderEntity orderEntity, LxMemberEntity memberEntity) {
        LxOrderPayEntity entity = new LxOrderPayEntity();
        entity.setPaySn(orderEntity.getPaySn());
        entity.setBuyerId(memberEntity.getMemberId());
        Session session = sessionFactory.openSession();

        session.save(entity);
        session.flush();
        session.close();
    }

    private long nextOrderSN() {
        lastOrderSN += 100;
        return lastOrderSN;
    }


    public void insertOrder(SessionFactory sessionFactory, LxMemberEntity memberEntity, LxGoodsEntity goodsEntity,
                            int addTime, int goodsCount) {
        LxOrderEntity entity = new LxOrderEntity();

        entity.setStoreId(goodsEntity.getStoreId());
        entity.setStoreName(goodsEntity.getStoreName());

        entity.setBuyerId(memberEntity.getMemberId());
        entity.setBuyerEmail(memberEntity.getMemberEmail());
        entity.setBuyerName(memberEntity.getMemberName());

        entity.setGoodsAmount(new BigDecimal(goodsEntity.getGoodsPrice().floatValue() * goodsCount));
        entity.setOrderAmount(new BigDecimal(goodsEntity.getGoodsPrice().floatValue() * goodsCount));
        entity.setPaymentCode("online");

        entity.setRcbAmount(BigDecimal.ZERO);
        entity.setCouponsAmount(BigDecimal.ZERO);
        entity.setPdAmount(BigDecimal.ZERO);

        entity.setOrderSn(nextOrderSN());
        Random random = new Random();
        byte state ;
        if (random.nextInt(10)==0){
             state = (byte)0;
        }else{
            state = (byte)40;
        };
        entity.setOrderState(state);

        lastPaySn = Util.nextPaySn(lastPaySn);
        entity.setPaySn(lastPaySn);

        entity.setAddTime(addTime);
        entity.setPaymentTime(addTime + new Random().nextInt(30 * 60) + 60);

        Session session = sessionFactory.openSession();
        session.save(entity);
        session.flush();
        session.close();

        entity = getLastOrderEntity(sessionFactory);
//        System.out.println(entity.getOrderId());


        insertOrderGoods(sessionFactory, memberEntity, goodsEntity, entity, goodsCount);
        insertOrderPay(sessionFactory, entity, memberEntity);
        insertOrderLog(entity);
    }


    private static void handleOneDay(SessionFactory sessionFactory, Date date, List<LxGoodsEntity> goods, List<LxMemberEntity> people) {
        List<Long> times = new LinkedList<>();
        for (int i = 0; i < 80; i++) {
            times.add(Util.getTimeStamp(date) / 1000);
        }
        Collections.sort(times);
        Random random = new Random();

        int[] ids = new int[80];
//        float sum = 0.0f;
        int goodsCount = random.nextInt(20)+40;
        for (int i = 0; i < ids.length; i++) {
            ids[i] = random.nextInt(goods.size());
//            sum += goods.get(ids[i]).getGoodsPrice().floatValue();
//            if (sum >150*50){
//                goodsCount = i;
//                break;
//            }
        }
        //  前两个直接加入系统, 如果两个的值小150, 再加一个

//        int idx = Math.min(10, (int) (170 / (sum / 10) + 2));
        System.out.println(goodsCount);
        OrderAdder orderAdder = new OrderAdder(sessionFactory);
        for (int i = 0; i < goodsCount; i++) {
            System.out.print(goods.get(ids[i]).getGoodsPrice().floatValue() + "\t");
            System.out.print(new Date(times.get(i) * 1000L) + "\t\t");
            int goodsIdx = ids[i];
            try {
                float onePrice = goods.get(ids[i]).getGoodsPrice().floatValue();
                int oneOrderCount = (int)((random.nextInt(30)+120)/onePrice+1);

                orderAdder.insertOrder(sessionFactory, people.get((goodsIdx % people.size())), goods.get(goodsIdx), (int) ((long) times.get(i)), oneOrderCount);
            }catch (Exception e){
//                e.printStackTrace();
            }
        }
    }

    private List <LxMemberEntity> getByuerList(int num, long time){

        Session session = mSessionFactory.openSession();
        Query query = session.createQuery("from LxMemberEntity E " + "ORDER BY E.memberTime DESC").setMaxResults(100);
        List<LxMemberEntity> orderEntity = query.list();
        session.close();
        return orderEntity;
    }

    // num 需要插入的数量, time 当时日期的时间基点
    public void handleOneDay(int num, long time){
        int[] ret = Util.normalDistTimePoint(num);
        List<LxMemberEntity> buyerList = getByuerList(num, time);
        Random random = new Random();
        for (int i = 0; i<ret.length; i++){
            boolean particularOrder =  random.nextInt(4)==0;
            int goodIdx = -1;
            int oneOrderCount = 1;

            if (particularOrder){
                float onePrice = 101;
                while(onePrice>100){
                    goodIdx = random.nextInt(goodsEntityList.size());
                    onePrice = goodsEntityList.get(goodIdx).getGoodsPrice().floatValue();
                }
            }else {
                goodIdx = random.nextInt(goodsEntityList.size());
                float onePrice = goodsEntityList.get(goodIdx).getGoodsPrice().floatValue();
                oneOrderCount = (int) ((random.nextInt(30) + 120) / onePrice + 1);
            }
            try{
            insertOrder(mSessionFactory, buyerList.get(random.nextInt(buyerList.size())),
                    goodsEntityList.get(goodIdx), (int)(time/1000+ret[i]), oneOrderCount);}
            catch (Exception e){

            }

            if(i%99==0){
                System.out.print(String.format("order:%d/%d\n", i, num));
            }
        }

    }
}
