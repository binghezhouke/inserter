package com.binghezhouk.main.OrmHelper;

import com.binghezhouk.main.Util;
import com.binghezhouke.entity.LxMemberEntity;
import com.binghezhouke.entity.LxOrderEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

/**
 * Created by binghezhouke on 16/7/10.
 */
public class UserAdder {
    private List<String> mNameAddressList;
    private List<String> mNamePasswdEmailList;
    private Random mRandom;

    public UserAdder(SessionFactory sessionFactory,String nameFile, String passwdFile,int offset) {
        mNameAddressList = Util.readFile(nameFile);
        mNamePasswdEmailList = Util.readFile(passwdFile);
        mRandom = new Random();

        String email = getLastEmail(sessionFactory);

        System.out.println(mNameAddressList.size());
        System.out.println(mNamePasswdEmailList.size());
        int idx = 0;
        for (String tmp:mNamePasswdEmailList){
            if(tmp.contains(email)){
                break;
            }else{
                idx++;
            }
        }
        if (idx>=mNamePasswdEmailList.size()){
            System.out.println("not found :");
            idx = 0;
        }else{
            System.out.println("found :" +idx);
        }
        offset = idx;
        for(int i=offset;i>0;i--){
           mNameAddressList.remove(0);
            mNamePasswdEmailList.remove(0);
        }
    }

    private static String getLastEmail(SessionFactory sessionFactory) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from LxMemberEntity E " + "ORDER BY E.memberId desc").setMaxResults(1);
        LxMemberEntity orderEntity = (LxMemberEntity) query.list().get(0);
        session.close();
        return orderEntity.getMemberEmail();
    }

    public synchronized void insertOneDayUser(SessionFactory sessionFactory, int num, long time) {
        int[] ret = Util.normalDistTimePoint(num);
        for (int i = 0; i < ret.length; i++) {
            Session session = sessionFactory.openSession();
            LxMemberEntity lxMemberEntity = new LxMemberEntity();

            String[] nameAddressPhone = null;
            while (true) {
                nameAddressPhone = mNameAddressList.remove(0).split(",");
                if (nameAddressPhone.length == 3) {
                    break;
                } else {
                    System.out.println("not valid data");
                }
            }
            ;
            String[] namePasswdEmail = null;
            while (true) {
                namePasswdEmail = mNamePasswdEmailList.remove(0).split("\t");
                if (namePasswdEmail.length == 3) {
                    break;
                } else {
                    System.out.println("not valid data");
                }
            }
            ;
            lxMemberEntity.setMemberTruename(nameAddressPhone[0]);
            lxMemberEntity.setMemberName(namePasswdEmail[0]);
            lxMemberEntity.setMemberPasswd(Util.MD5(namePasswdEmail[1]));
            lxMemberEntity.setMemberPaypwd(namePasswdEmail[1]);
            lxMemberEntity.setMemberEmail(namePasswdEmail[2]);
            lxMemberEntity.setMemberMobile(nameAddressPhone[2]);

            lxMemberEntity.setMemberMobileBind((byte) 1);
            lxMemberEntity.setMemberEmailBind((byte) 1);
            lxMemberEntity.setMemberTime((time / 1000 + ret[i]) + "");
            lxMemberEntity.setMemberLoginTime((time / 1000 + ret[i] + (mRandom.nextInt(5 * 60) + 30)) + "");
            lxMemberEntity.setMemberOldLoginTime((time / 1000 + ret[i] + (mRandom.nextInt(5 * 60) + 30)) + "");

            lxMemberEntity.setAvailablePredeposit(BigDecimal.ZERO);
            lxMemberEntity.setFreezePredeposit(BigDecimal.ZERO);
            lxMemberEntity.setAvailableRcBalance(BigDecimal.ZERO);
            lxMemberEntity.setFreezeRcBalance(BigDecimal.ZERO);

            if(i%99==0){
                System.out.print(String.format("user:%d/%d\n", i, num));
            }
            try {
                session.save(lxMemberEntity);
                session.flush();
            } catch (Exception e) {
//                e.printStackTrace();
            } finally {
                try {
                    session.close();
                } catch (Exception e) {
                }
            }
        }

    }
}
