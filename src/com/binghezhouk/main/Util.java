package com.binghezhouk.main;

import cern.jet.random.Distributions;
import cern.jet.random.Normal;
import cern.jet.random.engine.RandomEngine;

import java.io.*;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by binghezhouke on 16/7/8.
 */
public class Util {
    static int getlent2digital() {
        Random random = new Random();
        return random.nextInt(100000) % 88 + 10;
    }
    public  static long  nextPaySn(long current){
        int length = "740519485761323762".length();
        long base = (long)1E15;
        long last = current%base;
        int rand = getlent2digital();
        return (long)(rand*1E16) + last+new Random().nextInt(1000000);
    }

    static long getTimeStamp(String dateString){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = simpleDateFormat.parse(dateString);
            date.setHours(8);
            long min = date.getTime();
            date.setHours(20);
            long max = date.getTime();
            return new Random().nextInt((int)(max-min))+min;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }

    }

    public static long getTimeStamp(Date date){
            date.setHours(8);
            long min = date.getTime();
            date.setHours(20);
            long max = date.getTime();
            return new Random().nextInt((int)(max-min))+min;
    }

    static double[] get(int num){
        double[] ret = new double[num];
        RandomEngine randomEngine = RandomEngine.makeDefault();
        Normal normal = new Normal(5,0.5,randomEngine);
        for (int i=0; i<num ;i++) {
            ret [i] = normal.nextDouble();
        }
        return ret;
    }

    public static int[] normalDistTimePoint(int num) {

        // 9:30 11:00 15:00 17:00 20:00 22:00

        RandomEngine randomEngine = RandomEngine.makeDefault();

        int secondsPerDay = 24 * 60 * 60;
        int secondsPerHour = 60 * 60;
        int secondsPerMinute = 60;

        // 三个订单集中时间段的起始点,ab cd ef
        int a = 9 * secondsPerHour + 30 * secondsPerMinute;
        int b = 11 * secondsPerHour;

        int c = 15 * secondsPerHour;
        int d = 17 * secondsPerHour;

        int e = 20 * secondsPerHour;
        int f = 22 * secondsPerHour;


        Random random = new Random();

        // 0.7 忙时订单比较
        float time1 = 0.7f - (random.nextInt(20) - 10) / 100.0f;
        float time2 = 1 - 0.7f;


        // 忙时的时间段采用正态分布获取
        Normal normal = new Normal(time1 / 3, time1 / 60, randomEngine);
        int abCount = (int) (normal.nextDouble() * num);
        int cdCount = (int) (normal.nextDouble() * num);
        int efCount = (int) (normal.nextDouble() * num);
//        System.out.println(abCount + ":" + cdCount + ":" + efCount + ":" + (abCount + cdCount + efCount) + "/" + (num * time1));
        int elseCount = (int) (time2 * num);

        int[] ret = new int[abCount + cdCount + efCount + elseCount];
        int idx = 0;


        // 产生时间段 ab 的数据
        for (int i = 0; i < abCount; i++) {
            ret[idx++] = random.nextInt((b - a)) + a;
        }

        // 产生时间段  cd 的数据
        for (int i = 0; i < cdCount; i++) {
            ret[idx++] = random.nextInt((d - c)) + c;
        }

        // 产生时间段 ef 的数据
        for (int i = 0; i < efCount; i++) {
            ret[idx++] = random.nextInt((f - e)) + e;
        }

        // 产生剩余时间的数据

        // 闲时时段的起点, 闲时的秒数 为8-a b-c d-e f-24
        int timeStart[] = new int[]{8 * secondsPerHour, b, d, f};
        int timeEnd[] = new int[]{a, c, d, 24 * secondsPerHour};
        int timeLength[] = new int[timeEnd.length];
        //  闲时时段的总秒数
        int sumSecondsNormal = 0;
        for (int i = 0; i < timeStart.length; i++) {
            timeLength[i] = timeEnd[i] - timeStart[i];
            sumSecondsNormal += (timeLength[i]);
        }

        for (int i = 0; i < elseCount; i++) {
            int tmpIdx = random.nextInt(sumSecondsNormal);
            for (int j = 0; j < timeLength.length; j++) {
                tmpIdx -= timeLength[j];
                if (tmpIdx - timeLength[j] < 0) {
                    // 如果当前的值小于当前段的大小,那么代表该值应该属于该段
                    tmpIdx += timeStart[j];
                    break;
                } else {
                    tmpIdx -= timeLength[j];
                }
            }
            ret[idx++] = tmpIdx;
        }
        Arrays.sort(ret);
        return ret;
    }
    public static List<String> readFile(String fileName){
        List<String> fileContent = new LinkedList<>();
        FileInputStream fileInputStream = null;
            try {
                fileInputStream = new FileInputStream(fileName);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
                String tmp = null;
                while((tmp=bufferedReader.readLine())!=null){
                    fileContent.add(tmp);
                    if (fileContent.size()>120000){
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if(fileInputStream!=null){
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        return fileContent;
    }

    public static String MD5(String s) {
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        try {
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long strToDate(String str){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(str).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
